## full_k85v1_64-user 11 RP1A.200720.011 1625566115 release-keys
- Manufacturer: ulefone
- Platform: mt6785
- Codename: Power_Armor_13
- Brand: Ulefone
- Flavor: full_k85v1_64-user
- Release Version: 11
- Id: RP1A.200720.011
- Incremental: 1625566115
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Ulefone/Power_Armor_13_EEA/Power_Armor_13:11/RP1A.200720.011/1625566115:user/release-keys
- OTA version: 
- Branch: full_k85v1_64-user-11-RP1A.200720.011-1625566115-release-keys
- Repo: ulefone_power_armor_13_dump_6803


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
